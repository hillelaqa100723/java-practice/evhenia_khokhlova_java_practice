package Homework8CalculatorFile;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Homework8CalculatorFile {

        public enum Operation {
            PLUS("+"),
            MINUS("-"),
            DIV("/"),
            MULT("*");

            private String sign;

            Operation(String sign) {
                this.sign = sign;
            }

            public String getSign() {
                return sign;
            }
        }

    public static Double calculate(List<String> data) {
        if (data == null || data.size() != 3) {
            System.out.println("Invalid input data");
            return null;
        }

        double firstOperand = Double.parseDouble(data.get(0));
        double secondOperand = Double.parseDouble(data.get(2));
        String operationSign = data.get(1).trim();

        Operation operation = null;
        for (Operation op : Operation.values()) {
            if (op.getSign().equals(operationSign)) {
                operation = op;
                break;
            }
        }

        if (operation == null) {
            System.out.println("Invalid operation symbol");
            return null;
        }

        double result = 0;

        switch (operation) {
            case PLUS:
                result = firstOperand + secondOperand;
                break;
            case MINUS:
                result = firstOperand - secondOperand;
                break;
            case MULT:
                result = firstOperand * secondOperand;
                break;
            case DIV:
                if (secondOperand != 0) {
                    result = firstOperand / secondOperand;
                } else {
                    System.out.println("Cannot divide by zero");
                    return null;
                }
                break;
        }
        System.out.println();
        return result;
    }



        public static String prepareResultString(List<String> data, Double result) {
            String firstOperand = data.get(0);
            String operationSign = data.get(1).trim();
            String secondOperand = data.get(2);
            return firstOperand + " " + operationSign + " " + secondOperand + " = " + result;
        }




        public static void main (String[]args){

            List<String> data = readDataFromFile("Files/input.txt"); // Чтение данных из файла
            if (data != null) {
                Double result = calculate(data);
                if (result != null) {
                    System.out.println(prepareResultString(data, result));
                }
            }
        }

        public static List<String> readDataFromFile (String fileName){
            try {
                BufferedReader reader = new BufferedReader(new FileReader(fileName));
                String line = reader.readLine();
                reader.close();
                if (line != null) {
                    String[] parts = line.split(",");
                    if (parts.length == 3) {
                        List<String> data = new ArrayList<>();
                        for (String part : parts) {
                            data.add(part);
                        }
                        return data;
                    } else {
                        System.out.println("Invalid data format in the file");
                    }
                } else {
                    System.out.println("File is empty");
                }
            } catch (IOException e) {
                System.out.println("Error reading from file: " + e.getMessage());
            }
            return null;
        }
    }

