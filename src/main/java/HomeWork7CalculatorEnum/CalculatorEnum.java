package HomeWork7CalculatorEnum;

import java.util.List;

public class CalculatorEnum {
    public enum Operation {
        PLUS("+"),
        MINUS("-"),
        DIV("/"),
        MULT("*");

        private String sign;

        Operation(String sign) {
            this.sign = sign;
        }

        public String getSign() {
            return sign;
        }
    }

    public static Double calculate(List<String> data) {
        if (data == null || data.size() != 3) {
            System.out.println("Invalid input data");
            return null;
        }

        double firstOperand = Double.parseDouble(data.get(0));
        double secondOperand = Double.parseDouble(data.get(2));
        String operationSign = data.get(1).trim();

        Operation operation = null;
        for (Operation op : Operation.values()) {
            if (op.getSign().equals(operationSign)) {
                operation = op;
                break;
            }
        }

        if (operation == null) {
            System.out.println("Invalid operation symbol");
            return null;
        }

        double result = 0;

        switch (operation) {
            case PLUS:
                result = firstOperand + secondOperand;
                break;
            case MINUS:
                result = firstOperand - secondOperand;
                break;
            case MULT:
                result = firstOperand * secondOperand;
                break;
            case DIV:
                if (secondOperand != 0) {
                    result = firstOperand / secondOperand;
                } else {
                    System.out.println("Cannot divide by zero");
                    return null;
                }
                break;
        }

        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {
        String firstOperand = data.get(0);
        String operationSign = data.get(1).trim();
        String secondOperand = data.get(2);
        return firstOperand + " " + operationSign + " " + secondOperand + " = " + result;
    }
  public static void main(String[]Args){
          List<String> data = List.of("5.9", "*", "-5.0");
          System.out.println(prepareResultString(data,calculate(data)));
      }
  }
