package Lection5;

public class Array2 {
    public static int[] sumsInRows(int[][] originalArray) {
        int[] summ = new int[originalArray.length];
        for (int i = 0; i < originalArray.length; i++) {
            for (int j = 0; j < originalArray[i].length; j++) {
                summ[i]=summ[i]+originalArray[i][j];
            }
        }
        return summ;
    }
    public static void main(String[]args){
        int[][] originalArray =
                {
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9},
                };
        int[] rowSumm= sumsInRows(originalArray);
        for (int i : rowSumm) {
            System.out.println(i);
        }
    }
}