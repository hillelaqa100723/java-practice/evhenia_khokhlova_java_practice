package Lection8;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Homework9ProcessingData {
    public static Map<Integer, String> getDataFromFile(File dataFile) {
        Map<Integer, String> dataMap = new HashMap<>();

        try (Scanner scanner = new Scanner(dataFile)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(",");
                if (parts.length >= 2) {
                    int id = Integer.parseInt(parts[0]);
                    String value = parts[1];
                    dataMap.put(id, value);
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return dataMap;
    }

    public static String getDataById(Map<Integer, String> mapData, Integer id) {
        return mapData.get(id);
    }

    public static int getNumberOfOccurrences(Map<Integer, String> mapData, String lastName) {
        int count = 0;
        for (String value : mapData.values()) {
            String[] parts = value.split(" ");
            if (parts.length >= 1 && parts[0].equals(lastName)) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        File dataFile = new File("files/data.txt");
        Map<Integer, String> dataMap = getDataFromFile(dataFile);

        // Приклад
        Integer searchId = 2586;
        String dataForId = getDataById(dataMap, searchId);
        System.out.println("Data for ID " + searchId + ": " + dataForId);

        String searchLastName = "Ivanov";
        int occurrences = getNumberOfOccurrences(dataMap, searchLastName);
        System.out.println("Number of mentions of the last name " + searchLastName + ": " + occurrences);
    }

}
